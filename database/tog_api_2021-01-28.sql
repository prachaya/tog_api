-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 28, 2021 at 05:08 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tog_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_detail_tog_api`
--

CREATE TABLE `t_detail_tog_api` (
  `id` int(11) NOT NULL,
  `filereference` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `client` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `accn` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `do` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `lnam` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ptok` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `_rectype` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `acoat` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `prvm` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `prva` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `sph` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `cyl` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ax` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `add` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `mbase` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `_dateorder` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `_societe` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `_reference` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `_rucher` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `_numcli` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `_comment` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `_suppl` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `_lotnumber` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `_matrix` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `crib` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `_cto` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ipd` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ocht` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `minedg` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `omar` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `omal` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `circ` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `dbl` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `etyp` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ftyp` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `_lcoat` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `bsiz` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `lmattype` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `polish` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `fpd` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `mpd` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `fbfcin` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `fbfcup` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `_fbfcang` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `hbox` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `vbox` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_detail_tog_api`
--

INSERT INTO `t_detail_tog_api` (`id`, `filereference`, `client`, `accn`, `do`, `lnam`, `ptok`, `_rectype`, `acoat`, `prvm`, `prva`, `sph`, `cyl`, `ax`, `add`, `mbase`, `_dateorder`, `_societe`, `_reference`, `_rucher`, `_numcli`, `_comment`, `_suppl`, `_lotnumber`, `_matrix`, `crib`, `_cto`, `ipd`, `ocht`, `minedg`, `omar`, `omal`, `circ`, `dbl`, `etyp`, `ftyp`, `_lcoat`, `bsiz`, `lmattype`, `polish`, `fpd`, `mpd`, `fbfcin`, `fbfcup`, `_fbfcang`, `hbox`, `vbox`) VALUES
(1, '01044398', '1044398', '200373', 'B', '525N114;525N114', '1', 'N', 'C;C', '0.00;0.00', '0.00;0.00', '1.50;2.00', '0.00;0.00', '0;0', '2.50;2.50', '5.00;5.00', '20201015', 'NOVACEL OPHTALMIQUE', '0793601;0793602', 'H12', '66504', 'AKSESS PROG 1,6 ø70;AKSESS PROG 1,6 ø70', 'HMC; HMC', '20101507936', '240079361020101507936', '70;70', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2, '01044398', '1044398', '200373', 'B', '525N114;525N114', '1', 'N', 'C;C', '0.00;0.00', '0.00;0.00', '1.50;2.00', '0.00;0.00', '0;0', '2.50;2.50', '5.00;5.00', '20201015', 'NOVACEL OPHTALMIQUE', '0793601;0793602', 'H12', '66504', 'AKSESS PROG 1,6 ø70;AKSESS PROG 1,6 ø70', 'HMC; HMC', '20101507936', '240079361020101507936', '70;70', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(3, '01044398', '1044398', '200373', 'B', '525N114;525N114', '1', 'N', 'C;C', '0.00;0.00', '0.00;0.00', '1.50;2.00', '0.00;0.00', '0;0', '2.50;2.50', '5.00;5.00', '20201015', 'NOVACEL OPHTALMIQUE', '0793601;0793602', 'H12', '66504', 'AKSESS PROG 1,6 ø70;AKSESS PROG 1,6 ø70', 'HMC; HMC', '20101507936', '240079361020101507936', '70;70', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, '01044398', '1044398', '200373', 'B', '525N114;525N114', '1', 'N', 'C;C', '0.00;0.00', '0.00;0.00', '1.50;2.00', '0.00;0.00', '0;0', '2.50;2.50', '5.00;5.00', '20201015', 'NOVACEL OPHTALMIQUE', '0793601;0793602', 'H12', '66504', 'AKSESS PROG 1,6 ø70;AKSESS PROG 1,6 ø70', 'HMC; HMC', '20101507936', '240079361020101507936', '70;70', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(5, '01044398', '1044398', '200373', 'B', '525N114;525N114', '1', 'N', 'C;C', '0.00;0.00', '0.00;0.00', '1.50;2.00', '0.00;0.00', '0;0', '2.50;2.50', '5.00;5.00', '20201015', 'NOVACEL OPHTALMIQUE', '0793601;0793602', 'H12', '66504', 'AKSESS PROG 1,6 ø70;AKSESS PROG 1,6 ø70', 'HMC; HMC', '20101507936', '240079361020101507936', '70;70', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_header_tog_api`
--

CREATE TABLE `t_header_tog_api` (
  `id` int(11) NOT NULL,
  `companyname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `filereference` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_header_tog_api`
--

INSERT INTO `t_header_tog_api` (`id`, `companyname`, `filereference`) VALUES
(1, 'Novacel', '01044093'),
(2, 'Novacel', '01044093'),
(3, 'Novacel', '01044093'),
(4, 'Novacel', '01044093'),
(5, 'Novacel', '01044093'),
(6, 'Novacel', '01044093');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(1, 'admin', '123456');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_detail_tog_api`
--
ALTER TABLE `t_detail_tog_api`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_header_tog_api`
--
ALTER TABLE `t_header_tog_api`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_detail_tog_api`
--
ALTER TABLE `t_detail_tog_api`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_header_tog_api`
--
ALTER TABLE `t_header_tog_api`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
