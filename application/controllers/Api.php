<?php
defined('BASEPATH') or exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');


require APPPATH . 'libraries/REST_Controller.php';

class Api extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $username = $this->input->server('PHP_AUTH_USER');
        $password = $this->input->server('PHP_AUTH_PW');

        $this->load->model("UserModel");
        $this->UserModel->basicAuthen($username, $password);


        $this->load->model("ApiOrdersModel");
    }

    public function Orders_post()
    {
        $dataPost = json_decode($this->input->raw_input_stream, true);
        $data =  $this->ApiOrdersModel->InsertOrders($dataPost);
        $this->response($data);
    }
}
