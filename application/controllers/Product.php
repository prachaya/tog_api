<?php
defined('BASEPATH') or exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');


require APPPATH . 'libraries/REST_Controller.php';

class Product extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $username = $this->input->server('PHP_AUTH_USER');
        $password = $this->input->server('PHP_AUTH_PW');

        $this->load->model("UserModel");
        $this->UserModel->basicAuthen($username,$password);
           
        $this->load->model("ProductModel");
    }
    public function index_get($id = null)
    {
        $data =  $this->ProductModel->getProduct($id);
        $this->response($data);
    }
    public function index_post()
    {
        $dataPost = json_decode($this->input->raw_input_stream, true);
        $data =  $this->ProductModel->insertProduct($dataPost);
        $this->response($data);
    }
    public function insertList_post()
    {
        $dataPost = json_decode($this->input->raw_input_stream, true);
        $data =  $this->ProductModel->insertProductList($dataPost);
        $this->response($data);
    }
    public function updateList_post()
    {
        $dataPost = json_decode($this->input->raw_input_stream, true);
        $data =  $this->ProductModel->updateProductList($dataPost);
        $this->response($data);
    }
    public function deleteList_delete()
    {
        $dataPost = json_decode($this->input->raw_input_stream, true);
        $data =  $this->ProductModel->deleteProductList($dataPost);
        $this->response($data);
    }
}
