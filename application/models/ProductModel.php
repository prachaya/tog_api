<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ProductModel extends CI_Model
{

    private $table_name = "product";
    private $id = "id";

    public function getProduct($id)
    {
        $sql = "SELECT * FROM " . $this->table_name;

        if ($id) {
            $sql .= " WHERE id = '" . $id . "'";
        }

        $query = $this->db->query($sql);

        return $query->result_array();
    }
    public function insertProduct($data)
    {

        $product['name'] = isset($data['name']) ? $data['name'] : "";
        $product['price'] = isset($data['price']) ? $data['price'] : 0;
        $product['stock'] = isset($data['stock']) ? $data['stock'] : 0;

        if ($this->db->insert($this->table_name, $product)) {
            $res['status'] = true;
            $res['message'] = $this->db->insert_id();
            return  $res;
        } else {
            $res['status'] = false;
            $res['message'] = "Error has occurred";
            return  $res;
        }
    }
    public function insertProductList($data)
    {
        $count =  count($data);
        $res['number_of_record'] = 0;
        for ($i = 0; $i < $count; $i++) {
            $product['name'] = isset($data[$i]['name']) ? $data[$i]['name'] : "";
            $product['price'] = isset($data[$i]['price']) ? $data[$i]['price'] : 0;
            $product['stock'] = isset($data[$i]['stock']) ? $data[$i]['stock'] : 0;

            if ($this->db->insert($this->table_name, $product)) {
                $res['number_of_record']++;
                $res['message'][$i]['status'] = true;
                $res['message'][$i]['message'] = $this->db->insert_id();
            } else {
                $res['message'][$i]['status'] = false;
                $res['message'][$i]['message'] = "Error has occurred";
            }
        }
        return  $res;
    }
    public function updateProductList($data)
    {
        $count =  count($data);
        $res['number_of_update_record'] = 0;
        for ($i = 0; $i < $count; $i++) {

            $product=[];
            $product['id'] = isset($data[$i]['id']) ? $data[$i]['id'] : 0;

            // req all
            // $product['name'] = isset($data[$i]['name']) ? $data[$i]['name'] : "";
            // $product['price'] = isset($data[$i]['price']) ? $data[$i]['price'] : 0;
            // $product['stock'] = isset($data[$i]['stock']) ? $data[$i]['stock'] : 0;
            // some collumn
            // if(isset($data[$i]['name'])){
            //     $product['name']= $data[$i]['name'];
            // }

             isset($data[$i]['name'])? $product['name']= $data[$i]['name']:"";
             isset($data[$i]['price'])? $product['price']= $data[$i]['price']:"";
             isset($data[$i]['stock'])? $product['stock']= $data[$i]['stock']:"";

            $checkRecord = $this->getProduct($product['id']);
            if ($checkRecord) {
                if ($this->db->update($this->table_name, $product, array($this->id =>  $product['id']))) {
                    $res['number_of_update_record']++;
                    $res['message'][$i]['status'] = true;
                    $res['message'][$i]['message'] = $product['id'];
                } else {
                    $res['message'][$i]['status'] = false;
                    $res['message'][$i]['message'] = "Error has occurred";
                }
            } else {
                $res['message'][$i]['status'] = false;
                $res['message'][$i]['message'] = "Not found id" .$product['id'];
            }
        }
        return  $res;
    }
    public function deleteProductList($data)
    {
        $count =  count($data);
        $res['number_of_delete_record'] = 0;
        for ($i = 0; $i < $count; $i++) {
            $id = isset($data[$i]['id']) ? $data[$i]['id'] : 0;
 
            $checkRecord = $this->getProduct($id);
            if ($checkRecord) {
                if ($this->db->delete($this->table_name, array($this->id => $id)) ) {
                    $res['number_of_delete_record']++;
                    $res['message'][$i]['status'] = true;
                    $res['message'][$i]['message'] = "delete id: ".$id;
                } else {
                    $res['message'][$i]['status'] = false;
                    $res['message'][$i]['message'] = "Error has occurred";
                }
            } else {
                $res['message'][$i]['status'] = false;
                $res['message'][$i]['message'] = "Not found id " .$id;
            }
        }
        return  $res;
    }
}
