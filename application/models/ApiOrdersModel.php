<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ApiOrdersModel extends CI_Model
{

    private $id = "id";


    public function InsertOrders($data)
    {
        try {

            $orderHeader['id'] = 0;
            $orderHeader['companyname'] = isset($data['companyname']) ? $data['companyname'] : "";
            $orderHeader['filereference'] = isset($data['filereference']) ? $data['filereference'] : "";
            
            

            //required field//
            if ($orderHeader['companyname'] == "") {
                $result['message'] = "Company name is required";
                return $result;
            }

            if ($orderHeader['filereference'] == "") {
                $result['message'] = "Filereference is required";
                return $result;
            }
            //required field//

            //insert to table t_header_tog_api//
            if ($orderHeader['id'] == 0) {
                $nResult = $this->SQL_InsertHeaderOrders($orderHeader);
                if ($nResult > 0) {
                    $result['code'] = "200";
                    $result['message'] = "OK";
                } else {
                    $result['code'] = "400";
                    $result['message'] ="Lens not found";
                }
            }
            //insert to table t_header_tog_api//


            $orderList =  isset($data['ordercontent']) ? $data['ordercontent'] : "";

            if (!is_array($orderList) && count($orderList) == 0) {
                $result['message'] = "Ordercontent is required";
                return $result;
            }

            $count =  count($orderList);

            for ($i = 0; $i < $count; $i++) {

                $isInsert = true;

                $orderDetail['id'] = isset($orderList[$i]['id']) ? $orderList[$i]['id'] : 0;
                $orderDetail['filereference'] = isset($orderList[$i]['filereference']) ? $orderList[$i]['filereference'] : '';
                $orderDetail['client'] = isset($orderList[$i]['client']) ? $orderList[$i]['client'] : '';
                $orderDetail['accn'] = isset($orderList[$i]['accn']) ? $orderList[$i]['accn'] : '';
                $orderDetail['do'] = isset($orderList[$i]['do']) ? $orderList[$i]['do'] : '';
                $orderDetail['lnam'] = isset($orderList[$i]['lnam']) ? $orderList[$i]['lnam'] : '';
                $orderDetail['ptok'] = isset($orderList[$i]['ptok']) ? $orderList[$i]['ptok'] : '';
                $orderDetail['_rectype'] = isset($orderList[$i]['_rectype']) ? $orderList[$i]['_rectype'] : '';
                $orderDetail['acoat'] = isset($orderList[$i]['acoat']) ? $orderList[$i]['acoat'] : '';
                $orderDetail['prvm'] = isset($orderList[$i]['prvm']) ? $orderList[$i]['prvm'] : '';
                $orderDetail['prva'] = isset($orderList[$i]['prva']) ? $orderList[$i]['prva'] : '';
                $orderDetail['sph'] = isset($orderList[$i]['sph']) ? $orderList[$i]['sph'] : '';
                $orderDetail['cyl'] = isset($orderList[$i]['cyl']) ? $orderList[$i]['cyl'] : '';
                $orderDetail['ax'] = isset($orderList[$i]['ax']) ? $orderList[$i]['ax'] : '';
                $orderDetail['add'] = isset($orderList[$i]['add']) ? $orderList[$i]['add'] : '';
                $orderDetail['mbase'] = isset($orderList[$i]['mbase']) ? $orderList[$i]['mbase'] : '';
                $orderDetail['_dateorder'] = isset($orderList[$i]['_dateorder']) ? $orderList[$i]['_dateorder'] : '';
                $orderDetail['_societe'] = isset($orderList[$i]['_societe']) ? $orderList[$i]['_societe'] : '';
                $orderDetail['_reference'] = isset($orderList[$i]['_reference']) ? $orderList[$i]['_reference'] : '';
                $orderDetail['_rucher'] = isset($orderList[$i]['_rucher']) ? $orderList[$i]['_rucher'] : '';
                $orderDetail['_numcli'] = isset($orderList[$i]['_numcli']) ? $orderList[$i]['_numcli'] : '';
                $orderDetail['_comment'] = isset($orderList[$i]['_comment']) ? $orderList[$i]['_comment'] : '';
                $orderDetail['_suppl'] = isset($orderList[$i]['_suppl']) ? $orderList[$i]['_suppl'] : '';
                $orderDetail['_lotnumber'] = isset($orderList[$i]['_lotnumber']) ? $orderList[$i]['_lotnumber'] : '';
                $orderDetail['_matrix'] = isset($orderList[$i]['_matrix']) ? $orderList[$i]['_matrix'] : '';
                $orderDetail['crib'] = isset($orderList[$i]['crib']) ? $orderList[$i]['crib'] : '';
                $orderDetail['_cto'] = isset($orderList[$i]['_cto']) ? $orderList[$i]['_cto'] : '';
                $orderDetail['ipd'] = isset($orderList[$i]['ipd']) ? $orderList[$i]['ipd'] : '';
                $orderDetail['ocht'] = isset($orderList[$i]['ocht']) ? $orderList[$i]['ocht'] : '';
                $orderDetail['minedg'] = isset($orderList[$i]['minedg']) ? $orderList[$i]['minedg'] : '';
                $orderDetail['omar'] = isset($orderList[$i]['omar']) ? $orderList[$i]['omar'] : '';
                $orderDetail['omal'] = isset($orderList[$i]['omal']) ? $orderList[$i]['omal'] : '';
                $orderDetail['circ'] = isset($orderList[$i]['circ']) ? $orderList[$i]['circ'] : '';
                $orderDetail['dbl'] = isset($orderList[$i]['dbl']) ? $orderList[$i]['dbl'] : '';
                $orderDetail['etyp'] = isset($orderList[$i]['etyp']) ? $orderList[$i]['etyp'] : '';
                $orderDetail['ftyp'] = isset($orderList[$i]['ftyp']) ? $orderList[$i]['ftyp'] : '';
                $orderDetail['_lcoat'] = isset($orderList[$i]['_lcoat']) ? $orderList[$i]['_lcoat'] : '';
                $orderDetail['bsiz'] = isset($orderList[$i]['bsiz']) ? $orderList[$i]['bsiz'] : '';
                $orderDetail['lmattype'] = isset($orderList[$i]['lmattype']) ? $orderList[$i]['lmattype'] : '';
                $orderDetail['polish'] = isset($orderList[$i]['polish']) ? $orderList[$i]['polish'] : '';
                $orderDetail['fpd'] = isset($orderList[$i]['fpd']) ? $orderList[$i]['fpd'] : '';
                $orderDetail['mpd'] = isset($orderList[$i]['mpd']) ? $orderList[$i]['mpd'] : '';
                $orderDetail['fbfcin'] = isset($orderList[$i]['fbfcin']) ? $orderList[$i]['fbfcin'] : '';
                $orderDetail['fbfcup'] = isset($orderList[$i]['fbfcup']) ? $orderList[$i]['fbfcup'] : '';
                $orderDetail['_fbfcang'] = isset($orderList[$i]['_fbfcang']) ? $orderList[$i]['_fbfcang'] : '';
                $orderDetail['hbox'] = isset($orderList[$i]['hbox']) ? $orderList[$i]['hbox'] : '';
                $orderDetail['vbox'] = isset($orderList[$i]['vbox']) ? $orderList[$i]['vbox'] : '';

                //required field//
                if ($orderDetail['filereference'] == "") {
                    $result['ordercontent'][$i]['message'] = "Filereference is required (ไม่มีข้อมูลชุดที่ ".($i+1).")";
                    $isInsert = false;
                }
                if ($orderDetail['do'] == "") {
                    $result['ordercontent'][$i]['message'] = "Do is required (ไม่มีข้อมูลชุดที่ ".($i+1).")";
                    $isInsert = false;
                }
                if ($orderDetail['lnam'] == "") {
                    $result['ordercontent'][$i]['message'] = "Lnam is required (ไม่มีข้อมูลชุดที่ ".($i+1).")";
                    $isInsert = false;
                }
                if ($orderDetail['sph'] == "") {
                    $result['ordercontent'][$i]['message'] = "Sph is required (ไม่มีข้อมูลชุดที่ ".($i+1).")";
                    $isInsert = false;
                }
                if ($orderDetail['cyl'] == "") {
                    $result['ordercontent'][$i]['message'] = "Cyl is required (ไม่มีข้อมูลชุดที่ ".($i+1).")";
                    $isInsert = false;
                }
                if ($orderDetail['ax'] == "") {
                    $result['ordercontent'][$i]['message'] = "Ax is required (ไม่มีข้อมูลชุดที่ ".($i+1).")";
                    $isInsert = false;
                }
                if ($orderDetail['add'] == "") {
                    $result['ordercontent'][$i]['message'] = "Add is required (ไม่มีข้อมูลชุดที่ ".($i+1).")";
                    $isInsert = false;
                }
                if ($orderDetail['crib'] == "") {
                    $result['ordercontent'][$i]['message'] = "Crib is required (ไม่มีข้อมูลชุดที่ ".($i+1).")";
                    $isInsert = false;
                }
                //required field//

                 //insert to table t_detail_tog_api//
                if ($isInsert == true) {
                    if ($this->SQL_InsertDetailOrders($orderDetail)) {
                        $result['ordercontent'][$i]['code'] = "200";
                        $result['ordercontent'][$i]['message'] = "OK";
                        $result['ordercontent'][$i]['filereference'] = $orderDetail['filereference'];
                    } else {
                        $result['ordercontent'][$i]['code'] = "400";
                        $result['ordercontent'][$i]['message'] = "Lens not found";
                        $result['ordercontent'][$i]['filereference'] = $orderDetail['filereference'];
                    }
                }else{
                    $result['ordercontent'][$i]['code'] = "200";
                    $result['ordercontent'][$i]['filereference'] = $orderDetail['filereference'];
                }
                 //insert to table t_detail_tog_api//
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }

    public function SQL_InsertHeaderOrders($orderHeader)
    {
        $this->db->insert('t_header_tog_api', $orderHeader);

        return $this->db->insert_id();
    }

    public function SQL_InsertDetailOrders($orderDetail)
    {
        $this->db->insert('t_detail_tog_api', $orderDetail);

        return $this->db->insert_id();
    }
}
